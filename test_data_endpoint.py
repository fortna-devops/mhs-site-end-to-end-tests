# In this endpoint we verify:
# - if in the request auth header is missing - response of server should be unauthorized
# - the format of json is as expected
# - correct keys are on correct places
# - verify that metric is not an empty string and correct
# - verify that values keys in data section for sensors and plc not empty and correct

import datetime
from datetime import timedelta

from lib.base_mhs_aws import BaseMHSAWS


class TestDataEndpoint(BaseMHSAWS):

    def _get_location_data(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        url = self._site_url('sensors/index')
        loc_response = self._session.get(url)
        loc_resp = loc_response.json()
        loc_data = loc_resp['sensors']
        # create lists of sensors from location - expected data
        sensors = []
        for data in loc_data:
            sensor = data['sensor']
            sensors.append(sensor)
        sensors.sort()
        return sensors

    def test_get_data(self):
        # expected major keys
        major_keys = ['metric', 'data']
        major_keys.sort()

        # expected data section keys
        expected_data_keys = ['read_time', 'values']
        expected_data_keys.sort()

        # expected values keys for sensors
        expected_sensors_values = ['Z_RMS_IPS', 'X_RMS_V_IPS', 'TEMP_F']
        expected_sensors_values.sort()

        # expected values keys for plcs
        expected_plc_values = ['motor_freq', 'belt_speed', 'motor_current']
        expected_plc_values.sort()

        # start and end date definition
        end = datetime.datetime.now()
        end_date = end.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        start = end - timedelta(hours=42)
        start_date = start.strftime("%Y-%m-%dT%H:00:00.000Z")

        sensors, plcs = [], []
        for sensor in self._get_location_data():
            if sensor.startswith('PLC'):
                plcs.append(sensor)
                continue
            sensors.append(sensor)

        if len(sensors):
            sensor = sensors[0]
            url = self._site_url('data', sensor=sensor, metric='Z_RMS_IPS,X_RMS_V_IPS,TEMP_F',
                                 time1=start_date, time2=end_date, convert2metric=0)
            response_sensors = self._session.get(url)
            self.assertEqual(response_sensors.status_code, 200, 'sensor {}. Fail. Status code error: '.format(sensor))
            sensor_resp = response_sensors.json()
            self.assertTrue(isinstance(sensor_resp, dict), 'Fail! Wrong type of data!')

            # get actual major keys for sensors
            sensor_keys = list(sensor_resp.keys())
            sensor_keys.sort()

            # response metric type verification
            self.assertTrue(isinstance(sensor_resp['metric'], str), 'Fail. Wrong type of the data.')

            # response data type verification
            self.assertTrue(isinstance(sensor_resp['data'], list), 'Fail. Wrong type of the data.')

            # verify if keys actual == keys expected
            self.assertEqual(sensor_keys, major_keys, 'Fail. Wrong or missed keys!')

            # verify that metric is not an empty string
            self.assertNotIn(' ', sensor_resp['metric'], 'Fail. Metric should not be empty.')

            # verify data in metric for sensors
            self.assertEqual('Z_RMS_IPS,X_RMS_V_IPS,TEMP_F', sensor_resp['metric'],
                             'Fail. Wrong data in metric for sensor.')

            # verify that read_time and values exist in data section
            for sensor_data in sensor_resp['data']:
                sensor_data_keys = list(sensor_data.keys())
                sensor_data_keys.sort()

                # verify major sensor keys
                self.assertEqual(sensor_data_keys, expected_data_keys, 'Fail. No read_time in data section.')

                # verify values keys for sensors
                sensor_values = sensor_data['values']
                sensor_val_keys = list(sensor_values.keys())
                sensor_val_keys.sort()

                # verify that sensors values keys are not empty
                self.assertNotIn('', sensor_val_keys, 'Fail. Values keys for sensors should not be empty.')

                # verify that sensors values in data section are correct
                self.assertEqual(sensor_val_keys, expected_sensors_values, 'Fail. Wrong sensors values keys')

        if len(plcs):
            sensor = plcs[0]
            plc_url = self._site_url('data', sensor=sensor, metric='motor_freq,belt_speed,motor_current',
                                     time1=start_date, time2=end_date, convert2metric=0)
            response_plc = self._session.get(plc_url)
            self.assertEqual(response_plc.status_code, 200, 'sensor {}. Fail. Status code error: '.format(sensor))
            plc_resp = response_plc.json()
            self.assertTrue(isinstance(plc_resp, dict), 'Fail! Wrong type of data!')

            # get actual major keys for plc cards
            plc_keys = list(plc_resp.keys())
            plc_keys.sort()

            # response metric type verification
            self.assertTrue(isinstance(plc_resp['metric'], str), 'Fail. Wrong type of the data.')

            # response data type verification
            self.assertTrue(isinstance(plc_resp['data'], list), 'Fail. Wrong type of the data.')

            # verify if keys exist in response
            self.assertGreater(len(plc_keys), 0, 'Fail. No data in the response')

            # verify if keys actual == keys expected
            self.assertEqual(plc_keys, major_keys, 'Fail. Wrong or missed keys!')

            # verify that metric is not an empty string
            self.assertNotIn(' ', plc_resp['metric'], 'Fail. Metric should not be empty.')

            # verify data in metric for plc cards
            self.assertEqual('motor_freq,belt_speed,motor_current', plc_resp['metric'],
                             'Fail. Wrong data in metric for sensor.')

            # verify that read_time and values exist in data section
            for plc_data in plc_resp['data']:
                plc_data_keys = list(plc_data.keys())
                plc_data_keys.sort()
                # verify major plc keys
                self.assertEqual(plc_data_keys, expected_data_keys, 'Fail. No read_time in data section.')

                # verify values keys for PLCs
                plc_values = plc_data['values']
                plc_val_keys = list(plc_values.keys())
                plc_val_keys.sort()
                # verify that PLC values keys are not empty
                self.assertNotIn('', plc_val_keys, 'Fail. Values keys for PLCs should not be empty.')
                # verify that PLC values in data section are correct
                self.assertEqual(plc_val_keys, expected_plc_values, 'Fail. Wrong PLC values keys')
