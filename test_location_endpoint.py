# In this endpoint we verify:
# - if in the request auth header is missing - response of server should be unauthorized;
# - the format of json is as expected;
# - correct keys are on correct places;
# - 'facility organization' in 'data' section corresponds the name of 'conveyor';
# - 'thresholds' are not empty (except PLC thresholds data);
# - 'thresholds' corresponds the type of the equipment (except Amazon);
# - 'residual thresholds' are not empty (except Amazon);
# - 'residual thresholds' are correct;
# - json validation in thresholds and residual thresholds
import json

from lib.base_mhs_aws import BaseMHSAWS


class TestLocationEndpoint(BaseMHSAWS):
    def test_get_data(self):
        # expected keys for location endpoint
        expected_key = 'sensors'

        # list of expected major keys for each sensor - data section
        expected_data_keys = [
            'alarms_confidence',
            'conveyor',
            'description',
            'equipment',
            'facility_organization',
            'location',
            'm_sensor',
            'manufacturer',
            'residual_thresholds',
            'sensor',
            'standards_score',
            'temp_score',
            'thresholds',
            'vvrms_score'
        ]
        expected_data_keys.sort()

        expected_thresholds = ['temperature',
                               'rms_velocity_z',
                               'rms_velocity_x']
        expected_thresholds.sort()

        expected_thresholds_bearing = ['temperature',
                                       'rms_velocity_z',
                                       'rms_velocity_x',
                                       'hf_rms_acceleration_x',
                                       'hf_rms_acceleration_z']
        expected_thresholds_bearing.sort()

        expected_residual_thresholds = ['rms_velocity_x_residuals',
                                        'rms_velocity_z_residuals',
                                        'temperature_residuals']
        expected_residual_thresholds.sort()

        url = self._site_url('sensors/index')
        response = self._session.get(url)
        # status code verification
        self.assertEqual(response.status_code, 200, 'Fail. Status code error: ')
        # response in json format
        resp = response.json()
        # response type verification
        self.assertTrue(isinstance(resp, dict), 'Fail! Wrong type of the data in response - {}!'
                        .format(self._site))

        # gett actual major key from location
        major_key = ''.join(list(resp.keys()))

        # verify if major keys exist in response
        self.assertGreater(len(major_key), 0, 'Fail. No data in the response from {} location.'
                           .format(self._site))

        # verify if major keys actual == major keys expected
        self.assertEqual(major_key, expected_key, 'Fail. Wrong or missed keys in response from {} location.'
                         .format(self._site))

        # getting data from data section
        data_resp = resp['sensors']
        # data type verification in data section
        self.assertTrue(isinstance(data_resp, list), 'Fail. Wrong type of the data in data section of {}.'
                        .format(self._site))

        # verify list of the major keys for each sensor in data section
        for data in data_resp:
            # get facility organizations names
            facility_organization = data['facility_organization']
            # get sensors ids for fedex, dhl and amazon
            sensor = data['sensor']
            # get equipment type for further testing
            equipment_type = data['equipment']

            data_keys = list(data.keys())
            data_keys.sort()

            # verify if data exists in data section
            self.assertGreater(len(data_keys), 0, 'Fail. No data in response.')

            # verify if number of keys is correct in data section
            self.assertEqual(len(data_keys), len(expected_data_keys),
                             'Fail. {} - Wrong number of data section keys in response. Sensor {}.'
                             .format(self._site, sensor))

            # verify if list of actual data keys == expected keys
            self.assertEqual(data_keys, expected_data_keys,
                             'Fail. {} - Wrong data section keys for sensor {}.'.format(self._site, sensor))

            # get thresholds and residual_thresholds data
            thresholds_data = data['thresholds']
            residual_thresholds_data = data['residual_thresholds']
            alarms_confidence_data = data['alarms_confidence']

            # verify if data type in thresholds is correct
            self.assertTrue(isinstance(thresholds_data, str),
                            'Fail! {} - Wrong type of data in thresholds - sensor {}.'.format(self._site, sensor))

            # verify if data type in residual thresholds is correct
            self.assertTrue(isinstance(residual_thresholds_data, str),
                            'Fail! {} - Wrong type of data in residual thresholds - sensor {}.'
                            .format(self._site, sensor))

            # verify if data type in alarms_confidence is correct
            self.assertTrue(isinstance(alarms_confidence_data, str),
                            'Fail! {} - Wrong type of data in alarms_confidence - sensor {}.'
                            .format(self._site, sensor))

            # getting thresholds data for sensors from location
            # validation of json format
            try:
                sensors_thresholds = json.loads(thresholds_data)
            except ValueError as e:
                print('Invalid json: %s' % e)
                raise

            # getting residual thresholds data for sensors from location
            # validation of json format
            try:
                residual_thresholds = json.loads(residual_thresholds_data)
            except ValueError as er:
                print('Invalid json: %s' % er)
                raise

            # check threshold, residual_thresholds for each sensors except Ambient
            if facility_organization != 'Ambient Environment':
                # verify that data should not be empty for sensors
                if sensor.isdigit():
                    # verify type of data in thresholds
                    self.assertTrue(isinstance(sensors_thresholds, dict),
                                    '{} - Wrong type of thresholds data - sensor {}.'.format(self._site, sensor))
                    # verify if data exists in thresholds
                    self.assertGreater(len(sensors_thresholds), 0,
                                       'Fail. {} - thresholds should not be empty - sensor {}.'
                                       .format(self._site, sensor))

                    # create list of actual thresholds keys for sensors
                    sensor_thresholds_keys = list(sensors_thresholds.keys())
                    sensor_thresholds_keys.sort()

                    # verify keys (parameters) in thresholds for each sensor according the type of the equipment
                    if equipment_type != 'B':
                        self.assertEqual(sensor_thresholds_keys, expected_thresholds,
                                         'Fail. {} - Wrong thresholds keys for {} sensor {}. Keys should be {}.'
                                         .format(self._site, equipment_type, sensor, expected_thresholds))
                    else:
                        self.assertEqual(sensor_thresholds_keys, expected_thresholds_bearing,
                                         'Fail. {} - Wrong thresholds keys for {} sensor {}. Keys should be {}.'
                                         .format(self._site, equipment_type, sensor, expected_thresholds_bearing))
                else:
                    # verify type of data in thresholds
                    self.assertTrue(isinstance(sensors_thresholds, dict),
                                    '{} - Wrong type of thresholds data - sensor {}.'.format(self._site, sensor))

                    # verify that thresholds for PLC are empty strings
                    self.assertEqual(len(sensors_thresholds), 0,
                                     'Fail. {} - thresholds should be empty for PLC sensor {}.'
                                     .format(self._site, sensor))

                # verify type of data in residual thresholds
                self.assertTrue(isinstance(residual_thresholds, dict),
                                '{} - Wrong type of residual thresholds data for sensor {}.'
                                .format(self._site, sensor))

                # NO DATA in residual thresholds exists for dhl-milano for now
                if self._site != 'dhl-milano' and self._site != 'dhl-cincinnati':
                    # verify that data in residual thresholds exists
                    # - only fedex, dhl-miami and amazon
                    self.assertGreater(len(residual_thresholds), 0,
                                       'Fail. {} - residual thresholds should not be empty - sensor {}.'
                                       .format(self._site, sensor))

                    # create list of actual residual thresholds keys for sensors (except milano)
                    residual_thresholds_keys = list(residual_thresholds.keys())
                    residual_thresholds_keys.sort()

                    # verify keys (parameters) in residual thresholds for each sensor are correct
                    # - only fedex, dhl-miami and amazon
                    self.assertEqual(residual_thresholds_keys, expected_residual_thresholds,
                                     'Fail. {} - Wrong residual thresholds keys. Sensor {}. Keys should be {}.'
                                     .format(self._site, sensor, expected_residual_thresholds))
