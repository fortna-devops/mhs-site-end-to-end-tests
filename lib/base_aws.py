import os

import unittest
import requests


class BaseAWS(unittest.TestCase):

    @property
    def _auth_url(self):
        return os.getenv('AUTH_URL')

    @property
    def _auth_params(self):
        return {
            'client_id': os.getenv('AUTH_CLIENT_ID'),
            'response_type': 'code',
            'redirect_uri': os.getenv('AUTH_REDIRECT_URI')
        }

    @property
    def _auth_data(self):
        return {
            'username': os.getenv('AUTH_USERNAME'),
            'password': os.getenv('AUTH_PASSWORD')
        }

    def _auth(self):
        self._session = requests.Session()
        response = self._session.get(self._auth_url, params=self._auth_params)
        self.assertEqual(response.status_code, 200)

        data = self._auth_data
        data['_csrf'] = response.history[-1].headers['set-cookie'].lstrip('XSRF-TOKEN=').split(';', 1)[0]
        response = self._session.post(response.url, data=data)
        self.assertEqual(response.status_code, 200)

        self._session.headers.update({'Authorization': 'Bearer %s' % self._session.cookies['token']})
        self._session.close()
