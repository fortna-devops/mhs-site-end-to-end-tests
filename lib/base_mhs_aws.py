import os
import urllib.parse

from lib.base_aws import BaseAWS


class BaseMHSAWS(BaseAWS):

    @property
    def _auth_url(self):
        return os.getenv('AUTH_URL')

    @property
    def _auth_params(self):
        return {
            'client_id': os.getenv('AUTH_CLIENT_ID'),
            'response_type': 'code',
            'redirect_uri': os.getenv('AUTH_REDIRECT_URI')
        }

    @property
    def _base_url(self):
        return os.getenv('BASE_URL')

    @property
    def _site(self):
        return os.getenv('SITE')

    @property
    def _customer(self):
        return self._site.split('-')[0]

    def _url(self, url, **params):
        url = urllib.parse.urljoin(self._base_url, url)
        if not params:
            return url
        return f"{url}?{urllib.parse.urlencode(params)}"

    def _site_url(self, url='', **params):
        params['site'] = self._site
        return self._url(url, **params)

    def _customer_url(self, url='', **params):
        params['customer'] = self._customer
        return self._url(url, **params)

    def setUp(self):
        self._auth()

    def tearDown(self):
        self._session.close()
