# In this endpoint we verify:
# - if in the request auth header is missing - response of server should be unauthorized
# - we have menu with 4 levels for dhl, and 2 levels for amazon and fedex
# - total quantity of facility organization is equal to uniq values of facility_organization in location request
# - we can see all names of conveyors on the left-menu (the total list is received from location request)

from lib.base_mhs_aws import BaseMHSAWS


class TestLeftMenu(BaseMHSAWS):

    def _get_facilities_from_location(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        url = self._site_url('sensors/index')
        response = self._session.get(url)
        # status code verification
        self.assertEqual(response.status_code, 200, 'Fail. Status code error: ')
        # response in json format
        location_response = response.json()
        # verify if data exists in response from location
        self.assertGreater(len(location_response), 0, 'Fail! No data in response!')
        # get all data from location
        location_data = location_response['sensors']
        facility_organization = []
        for data in location_data:
            facility_org = data['facility_organization']
            if facility_org != 'Ambient Environment':
                facility_organization.append(facility_org)
        # make unique facility_organizations names list
        facility_organizations = list(set(facility_organization))
        facility_organizations.sort()
        # expected list of facility organizations
        return facility_organizations

    def _get_conveyors_from_location(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        url = self._site_url('sensors/index')
        response = self._session.get(url)
        # status code verification
        self.assertEqual(response.status_code, 200, 'Fail. Status code error: ')
        # response in json format
        location_response = response.json()
        # verify if data exists in response from location
        self.assertGreater(len(location_response), 0, 'Fail! No data in response!')
        # get all data from location
        location_data = location_response['sensors']
        conveyors_list = []
        for data in location_data:
            conveyor = data['conveyor']
            facility_org = data['facility_organization']
            if facility_org != 'Ambient Environment':
                conveyors_list.append(conveyor)
        # make unique conveyors names list
        conveyors = list(set(conveyors_list))
        conveyors.sort()
        # expected list of conveyors
        return conveyors

    def test_get_data(self):
        # lists of major expected keys
        expected_customer_keys = {'data', 'levels'}

        # list of expected major overview data keys
        expected_overview_data_keys = ['critical_conveyors', 'facilities', 'moderate_conveyors']
        expected_overview_data_keys.sort()

        # list of expected overview section keys from mainRegions section
        expected_overview_sect_keys = ['area_percentages', 'moderate_conveyors', 'total_percentage',
                                       'critical_conveyors']
        expected_overview_sect_keys.sort()

        # list of expected facility keys for each site
        expected_cust_facility_keys = ['name', 'site', 'color', 'conveyorData', 'overview']
        expected_cust_facility_keys.sort()

        # facility keys in overview section
        expected_ov_facilit_data_keys = ['color', 'critical', 'moderate', 'name', 'normal', 'num_critical_conveyors',
                                         'num_moderate_conveyors', 'num_normal_conveyors']
        expected_ov_facilit_data_keys.sort()

        # list of expected conveyors section keys for each site
        expected_cust_conveyor_sect_keys = ['color', 'conveyors', 'name', 'num_critical_sensors',
                                            'num_moderate_sensors', 'num_normal_sensors', 'num_sensors']
        expected_cust_conveyor_sect_keys.sort()

        # verify facilities keys in region section of 4-levels menu
        expected_facilities_data_keys = ['color', 'conveyorData', 'latitude', 'longitude', 'name', 'overview', 'site']
        expected_facilities_data_keys.sort()

        # countries data keys for 4-levels menu
        expected_countries_data_keys = ['color', 'mainRegions', 'name', 'overview']
        expected_countries_data_keys.sort()

        # mainRegion jeys of 4-levels menu
        expected_regions_data_keys = ['color', 'facilities', 'name', 'overview']
        expected_regions_data_keys.sort()

        # expected keys of each conveyor
        expected_cust_conveyor_keys = ['color', 'name']
        expected_cust_conveyor_keys.sort()

        # list of common urgency keys
        common_urg_keys = ['critical', 'moderate', 'normal']
        common_urg_keys.sort()

        # data from location endpoint
        facility_organizations = self._get_facilities_from_location()
        conveyors = self._get_conveyors_from_location()

        url = self._customer_url('left-menu')
        response = self._session.get(url)
        # status code verification
        self.assertEqual(response.status_code, 200, 'Fail. Status code error: ')
        resp = response.json()
        # verify if data exist in response
        self.assertGreater(len(resp), 0, 'Fail! No data in response!')
        # response type verification
        self.assertTrue(isinstance(resp, dict), 'Fail! Wrong type of the data in response!')

        # getting customer from the the response
        actual_customer = list(resp.keys())
        # verify if customer exists in response
        self.assertGreater(len(actual_customer), 0, 'Fail! No customer in response.')

        # get all data for customer
        cust_data = resp[self._customer]
        self.assertTrue(isinstance(cust_data, dict), 'Fail! Wrong type of the data!')

        # get major keys for the customer
        cust_keys = cust_data.keys()
        # verify if actual keys == expected keys
        self.assertEqual(cust_keys, expected_customer_keys,
                         'Fail! Wrong keys or keys do not exist for the customer')

        cust_group_data = cust_data['data']
        # verify type of data for customer
        self.assertTrue(isinstance(cust_group_data, dict),
                        'Fail! Wrong type of the data for the customer')

        # MENU LEVELS VERIFICATION
        # verify type of levels value for customer
        self.assertTrue(isinstance(cust_data['levels'], int),
                        'Fail! Type of the menu levels should be integer | number')

        # two levels menu verification
        if cust_data['levels'] == 2:
            # verify that facilities key exists in customer data section
            self.assertIn("facilities", cust_group_data,
                          'Fail. No \'facilities\' key in the left-menu data section.')

            cust_facility_data = cust_group_data['facilities']
            for facilities_data in cust_facility_data:
                cust_facility_keys = list(facilities_data.keys())
                cust_facility_keys.sort()
                # verify that all keys exist and correct in the facilities section
                self.assertEqual(cust_facility_keys, expected_cust_facility_keys,
                                 '{}. Fail, wrong keys in the facilities section. Keys should be {}'
                                 .format(self._site, expected_cust_facility_keys))

                # check facility data of the site
                if facilities_data['site'] == self._site:
                    # get facility organizations, conveyors values from conveyorData section
                    cust_conv_section_data = facilities_data['conveyorData']
                    # facility organizations list
                    cust_facility_organizations = []
                    # conveyors list
                    cust_conveyors = []
                    for cust_data in cust_conv_section_data:
                        cust_conveyor_sect_keys = list(cust_data.keys())
                        cust_conveyor_sect_keys.sort()
                        # check that keys for critical, moderate and normal sensors exist
                        self.assertEqual(cust_conveyor_sect_keys, expected_cust_conveyor_sect_keys,
                                         '{}. Fail. Incorrect keys in the conveyorData section. Should be {}'
                                         .format(self._site, expected_cust_conveyor_sect_keys))

                        # get names of facility organizations
                        cust_facility_orgs = cust_data['name']
                        # list of facility organizations from the left menu
                        cust_facility_organizations.append(cust_facility_orgs)
                        # get conveyors data
                        cust_conveyors_data = cust_data['conveyors']
                        for cust_conveyor in cust_conveyors_data:
                            # check keys for each conveyor
                            cust_conveyor_keys = list(cust_conveyor.keys())
                            cust_conveyor_keys.sort()
                            self.assertEqual(cust_conveyor_keys, expected_cust_conveyor_keys,
                                             '{}. Fail. Incorrect keys of the conveyor. Should be {}'
                                             .format(self._site, expected_cust_conveyor_keys))
                            # get name of each conveyor
                            cust_conv_name = cust_conveyor['name']
                            # list of conveyors from the left menu
                            cust_conveyors.append(cust_conv_name)
                    cust_conveyors.sort()
                    cust_facility_organizations.sort()

                    # FACILITIES QUANTITY AND VALUES VERIFICATION
                    # verify that facility organizations are not empty
                    self.assertNotIn('', cust_facility_organizations,
                                     'Fail. Facility organization value should not be empty.')

                    # verify if number of facility organizations for is correct
                    self.assertEqual(len(cust_facility_organizations), len(facility_organizations),
                                     'Fail. Wrong number of facility organizations')

                    # verify if names of facility organizations for is correct
                    self.assertEqual(cust_facility_organizations, facility_organizations,
                                     'Fail. Incorrect names of facility organizations')

                    # CONVEYORS QUANTITY AND NAMES VERIFICATION
                    # verify that conveyors are not empty
                    self.assertNotIn('', cust_conveyors,
                                     'Fail. Facility organization value should not be empty.')

                    # verify if quantity of conveyors of the customer is correct
                    self.assertEqual(len(cust_conveyors), len(conveyors),
                                     'Fail. Wrong number of conveyors')

                    # verify if names of conveyors of the customer are correct
                    self.assertEqual(cust_conveyors, conveyors,
                                     'Fail. Incorrect names of conveyors. Names should be the same as in location')

                    # verify data from overview section
                    cust_overview_sect_data = facilities_data['overview']
                    cust_overview_sect_keys = list(cust_overview_sect_data.keys())
                    cust_overview_sect_keys.sort()
                    self.assertEqual(cust_overview_sect_keys, expected_overview_sect_keys,
                                     '{}. Incorrect keys or not keys exist in overview section. Expected keys {}'
                                     .format(self._site, expected_overview_sect_keys))

                    area_perc_data = cust_overview_sect_data['area_percentages']
                    area_perc_keys = list(area_perc_data.keys())
                    area_perc_keys.sort()
                    self.assertEqual(area_perc_keys, facility_organizations,
                                     '{}. Fail, wrong keys in section. Keys should be the same as facility orgs {}'
                                     .format(self._site, facility_organizations))

                    total_perc_data = cust_overview_sect_data['total_percentage']
                    total_perc_keys = list(total_perc_data.keys())
                    total_perc_keys.sort()
                    self.assertEqual(total_perc_keys, common_urg_keys,
                                     '{}. Fail, wrong keys in section. Keys should be {}'
                                     .format(self._site, common_urg_keys))

                    # check critical and moderate conveyors keys if data exist
                    critical_conv_data = cust_overview_sect_data['critical_conveyors']
                    moderate_conv_data = cust_overview_sect_data['moderate_conveyors']

                    if len(critical_conv_data):
                        for cr_conv_data in critical_conv_data:
                            cr_conv_data_keys = list(cr_conv_data.keys())
                            cr_conv_data_keys.sort()
                            self.assertEqual(cr_conv_data_keys, expected_cust_conveyor_keys,
                                             '{}. Fail, incorrect critical conveyor keys. Should be {}.'
                                             .format(self._site, expected_cust_conveyor_keys))

                    if len(moderate_conv_data):
                        for md_conv_data in moderate_conv_data:
                            md_conv_data_keys = list(md_conv_data.keys())
                            md_conv_data_keys.sort()
                            self.assertEqual(md_conv_data_keys, expected_cust_conveyor_keys,
                                             '{}. Fail, incorrect moderate conveyor keys. Should be {}'
                                             .format(self._site, expected_cust_conveyor_keys))

        else:
            # verify number of levels for the left menu (4-levels-menu)
            self.assertEqual(cust_data['levels'], 4,
                             'Fail! Wrong number of levels for the left menu.')
            # verify if countries key exists in left-menu data section
            self.assertIn("countries", cust_group_data, 'Fail. No \'countries\' key in the left-menu data section.')

            # verify major keys in countries section
            cust_countries_data = cust_group_data['countries']
            for countries_data in cust_countries_data:
                countries_data_keys = list(countries_data.keys())
                countries_data_keys.sort()
                self.assertEqual(countries_data_keys, expected_countries_data_keys,
                                 '{}. Fail. Incorrect countries data keys. Keys should be {}.'
                                 .format(self._site, expected_countries_data_keys))

                # verify major keys in overview section of the country
                cust_overview_data = countries_data['overview']
                cust_overview_data_keys = list(cust_overview_data.keys())
                cust_overview_data_keys.sort()
                self.assertEqual(cust_overview_data_keys, expected_overview_data_keys,
                                 '{}. Incorrect keys or not keys exist in overview section. Expected keys {}'
                                 .format(self._site, expected_overview_data_keys))

                # verify critical and moderate conveyors keys in overview section
                overview_cr_conveyors = cust_overview_data['critical_conveyors']
                overview_md_conveyors = cust_overview_data['moderate_conveyors']
                if len(overview_cr_conveyors):
                    for critical_conv_data in overview_cr_conveyors:
                        critical_conv_data_keys = list(critical_conv_data.keys())
                        critical_conv_data_keys.sort()
                        self.assertEqual(critical_conv_data_keys, expected_cust_conveyor_keys,
                                         '{}. Fail, incorrect keys. Keys should be {}'
                                         .format(self._site, expected_cust_conveyor_keys))
                if len(overview_md_conveyors):
                    for moderate_conv_data in overview_md_conveyors:
                        moderate_conv_data_keys = list(moderate_conv_data.keys())
                        moderate_conv_data_keys.sort()
                        self.assertEqual(moderate_conv_data_keys, expected_cust_conveyor_keys,
                                         '{}. Fail, Incorrect keys. Keys should be {}'
                                         .format(self._site, expected_cust_conveyor_keys))

                overview_facilit_data = cust_overview_data['facilities']
                for facilit_data in overview_facilit_data:
                    ov_facilit_data_keys = list(facilit_data.keys())
                    ov_facilit_data_keys.sort()
                    self.assertEqual(ov_facilit_data_keys, expected_ov_facilit_data_keys,
                                     '{}. Fail, incorrect facility keys in overview section. Should be {}'
                                     .format(self._site, expected_ov_facilit_data_keys))

                # verify keys in mainRegions section
                cust_region_data = countries_data['mainRegions']
                for regions_data in cust_region_data:
                    # verify keys in mainRegions section
                    regions_data_keys = list(regions_data.keys())
                    regions_data_keys.sort()
                    # verify mainRegion section data keys
                    self.assertEqual(regions_data_keys, expected_regions_data_keys,
                                     '{}. Fail. Incorrect mainRegion keys. KEys should be {}.'
                                     .format(self._site, expected_regions_data_keys))
                    # get customer facilities from region data section
                    cust_facility_data = regions_data['facilities']
                    for facilities_data in cust_facility_data:
                        facilities_data_keys = list(facilities_data.keys())
                        facilities_data_keys.sort()
                        # verify that all keys exist in the facilities section - 4level menu
                        self.assertEqual(facilities_data_keys, expected_facilities_data_keys,
                                         '{}. Fail. Incorrect facility keys in region data section. Should be {}'
                                         .format(self._site, expected_facilities_data_keys))

                        # check facility data of the site
                        if facilities_data['site'] == self._site:
                            # get facility organizations, conveyors values from conveyorData section
                            cust_conv_section_data = facilities_data['conveyorData']
                            # facility organizations list
                            cust_facility_organizations = []
                            # conveyors list
                            cust_conveyors = []
                            for cust_data in cust_conv_section_data:
                                cust_conveyor_sect_keys = list(cust_data.keys())
                                cust_conveyor_sect_keys.sort()
                                # check that keys for critical, moderate and normal sensors exist
                                self.assertEqual(cust_conveyor_sect_keys, expected_cust_conveyor_sect_keys,
                                                 '{}. Fail. Incorrect keys in the conveyorData section. Should be {}'
                                                 .format(self._site, expected_cust_conveyor_sect_keys))

                                # get names of facility organizations
                                cust_facility_orgs = cust_data['name']
                                # list of facility organizations from the left menu
                                cust_facility_organizations.append(cust_facility_orgs)
                                # get conveyors data
                                cust_conveyors_data = cust_data['conveyors']
                                for cust_conveyor in cust_conveyors_data:
                                    # check keys for each conveyor
                                    cust_conveyor_keys = list(cust_conveyor.keys())
                                    cust_conveyor_keys.sort()
                                    self.assertEqual(cust_conveyor_keys, expected_cust_conveyor_keys,
                                                     '{}. Fail. Incorrect keys of the conveyor. Should be {}'
                                                     .format(self._site, expected_cust_conveyor_keys))
                                    # get name of each conveyor
                                    cust_conv_name = cust_conveyor['name']
                                    # list of conveyors from the left menu
                                    cust_conveyors.append(cust_conv_name)
                            cust_conveyors.sort()
                            cust_facility_organizations.sort()

                            # FACILITIES QUANTITY AND VALUES VERIFICATION
                            # verify that facility organizations are not empty
                            self.assertNotIn('', cust_facility_organizations,
                                             'Fail. Facility organization value should not be empty.')

                            # verify if number of facility organizations for is correct
                            self.assertEqual(len(cust_facility_organizations), len(facility_organizations),
                                             'Fail. Wrong number of facility organizations')

                            # verify if names of facility organizations for is correct
                            self.assertEqual(cust_facility_organizations, facility_organizations,
                                             'Fail. Incorrect names of facility organizations')

                            # CONVEYORS QUANTITY AND NAMES VERIFICATION
                            # verify that conveyors are not empty
                            self.assertNotIn('', cust_conveyors,
                                             'Fail. Facility organization value should not be empty.')

                            # verify if quantity of conveyors of the customer is correct
                            self.assertEqual(len(cust_conveyors), len(conveyors),
                                             'Fail. Wrong number of conveyors')

                            # verify if names of conveyors of the customer are correct
                            self.assertEqual(cust_conveyors, conveyors,
                                             'Fail. Incorrect names of conveyors. '
                                             'Names should be the same as in location')

                            # verify data from overview section
                            cust_overview_sect_data = facilities_data['overview']
                            cust_overview_sect_keys = list(cust_overview_sect_data.keys())
                            cust_overview_sect_keys.sort()
                            self.assertEqual(cust_overview_sect_keys, expected_overview_sect_keys,
                                             '{}. Incorrect keys or not keys exist in overview section. '
                                             'Expected keys {}'
                                             .format(self._site, expected_overview_sect_keys))

                            area_perc_data = cust_overview_sect_data['area_percentages']
                            area_perc_keys = list(area_perc_data.keys())
                            area_perc_keys.sort()
                            self.assertEqual(area_perc_keys, facility_organizations,
                                             '{}. Fail, wrong keys in section. '
                                             'Keys should be the same as facility orgs {}'
                                             .format(self._site, facility_organizations))

                            total_perc_data = cust_overview_sect_data['total_percentage']
                            total_perc_keys = list(total_perc_data.keys())
                            total_perc_keys.sort()
                            self.assertEqual(total_perc_keys, common_urg_keys,
                                             '{}. Fail, wrong keys in section. Keys should be {}'
                                             .format(self._site, common_urg_keys))

                            # check critical and moderate conveyors keys if data exist
                            critical_conv_data = cust_overview_sect_data['critical_conveyors']
                            moderate_conv_data = cust_overview_sect_data['moderate_conveyors']
                            if len(critical_conv_data):
                                for cr_conv_data in critical_conv_data:
                                    cr_conv_data_keys = list(cr_conv_data.keys())
                                    cr_conv_data_keys.sort()
                                    self.assertEqual(cr_conv_data_keys, expected_cust_conveyor_keys,
                                                     '{}. Fail, incorrect critical conveyor keys. Should be {}.'
                                                     .format(self._site, expected_cust_conveyor_keys))
                            if len(moderate_conv_data):
                                for md_conv_data in moderate_conv_data:
                                    md_conv_data_keys = list(md_conv_data.keys())
                                    md_conv_data_keys.sort()
                                    self.assertEqual(md_conv_data_keys, expected_cust_conveyor_keys,
                                                     '{}. Fail, incorrect moderate conveyor keys. Should be {}'
                                                     .format(self._site, expected_cust_conveyor_keys))
