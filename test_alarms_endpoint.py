# In this endpoint we verify:
# - if in the request auth header is missing - response of server should be unauthorized;
# - the format of json is as expected;
# - correct keys are on correct places;
# - data in 'remedies' section should exists if alarms are not empty and v.v..
# - quantity of messages in 'remedies' sections corresponds quantity of alarms;
import random

from lib.base_mhs_aws import BaseMHSAWS


class TestAlarms(BaseMHSAWS):

    def _get_location_data(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        url = self._site_url('sensors/index')
        response = self._session.get(url)
        self.assertEqual(response.status_code, 200, 'Fail. Status code error: ')
        location_response = response.json()  # response in json format
        # verify if data exists in response from location
        self.assertGreater(len(location_response), 0, 'Fail! No data in response!')
        # get all data from location
        location_data = location_response['sensors']
        # create lists of conveyors from location - expected data
        conveyors = []
        for data in location_data:
            conveyor = data['conveyor']
            conveyors.append(conveyor)
        conveyors.sort()
        return conveyors

    def test_get_data(self):
        conveyors = self._get_location_data()
        conveyor = random.choice(conveyors)
        self.assertIsNot(conveyor, '', 'Fail. Conveyor should not be empty.')

        # lists of expected keys for alarms section
        expected_major_keys = ['alarms', 'remedies']
        expected_major_keys.sort()

        expected_alarms_keys = ['actual_value', 'conveyor', 'equipment_type', 'key_label', 'sensor', 'severity',
                                'tag_limit', 'tag_name', 'timestamp']
        expected_alarms_keys.sort()

        # VERIFICATION OF ALARMS AND REMEDIES
        url = self._site_url('alarms', conveyor=conveyor)
        response = self._session.get(url)
        # status code verification
        self.assertEqual(response.status_code, 200, 'Fail. Status code error: ')
        resp = response.json()
        # type verification
        self.assertTrue(isinstance(resp, dict), 'Fail! Wrong type of data!')

        # get major keys for alarms from response
        alarm_keys = list(resp.keys())
        alarm_keys.sort()
        self.assertEqual(alarm_keys, expected_major_keys, 'Fail. Wrong keys or keys do not exists.')

        # get alarms data from response
        alarm_data = resp['alarms']
        # get remedies data from response
        remedies_data = resp['remedies']

        # verify type of data in response
        self.assertTrue(isinstance(alarm_data, list), 'Fail! Wrong type of data in alarms section!')
        self.assertTrue(isinstance(remedies_data, list), 'Fail! Wrong type of data in remedies section!')

        # verification of remedies data if no alarms
        if len(alarm_data) == 0:
            self.assertEqual(len(remedies_data), 0, 'Fail. Remedies section should be empty, because no alarms.')

        # verify that data in remedies section should exist when alarms exist
        elif len(alarm_data) > 0:
            self.assertGreater(len(remedies_data), 0, 'Fail. Remedies section should not be empty, '
                                                      'because alarms exist.')

            # verify that number of messages corresponds number of alarms
            self.assertEqual(len(remedies_data), len(alarm_data), 'Fail. Quantity of messages should correspond to '
                                                                  'the number of alarms.')

            # verification of alarms section keys
            for alarm in alarm_data:
                alarms_keys = list(alarm.keys())
                alarms_keys.sort()
                self.assertEqual(alarms_keys, expected_alarms_keys, 'Fail. Wrong keys or keys do not exists.')

            # verification of remedies values types if exist
            if len(remedies_data) > 0:
                for message in remedies_data:
                    self.assertTrue(isinstance(message, str), 'Fail. Wrong type of data, message should be string.')
