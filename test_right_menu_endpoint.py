# In this endpoint we verify:
# - if in the request auth header is missing - response of server should be unauthorized;
# - the format of json is as expected;
# - correct keys are on correct places;
# - there are 3 sections available - critical, need_attention and normal;
# - list of keys for each sensor (only if data exists in corresponding sections);
# - thresholds exists for each sensor;
# - thresholds for PLCs are empty
# - parameters (keys) in thresholds according the type of equipment (differ for Bearing);
# - urgency keys (red and orange) exist for each parameter and are correct.
import json
import random

from lib.base_mhs_aws import BaseMHSAWS


class TestRightMenu(BaseMHSAWS):

    def _get_location_data(self):
        # GETTING LOCATION DATA
        url = self._site_url('sensors/index')
        response = self._session.get(url)
        self.assertEqual(response.status_code, 200, 'Fail. Status code error: ')
        location_response = response.json()
        self.assertGreater(len(location_response), 0, 'Fail! No data in response!')
        location_data = location_response['sensors']
        return location_data

    def _get_conveyors_from_location(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        location_data = self._get_location_data()
        conveyors_list = []
        for data in location_data:
            conveyor = data['conveyor']
            facility_org = data['facility_organization']
            if facility_org != 'Ambient Environment':
                conveyors_list.append(conveyor)
        # make unique conveyors names list
        conveyors = list(set(conveyors_list))
        conveyors.sort()
        # expected list of conveyors
        return conveyors

    def _get_urg_params_from_location(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        location_data = self._get_location_data()
        urg_keys = []
        for data in location_data:
            if data['facility_organization'] != 'Ambient Environment':
                # json validation
                try:
                    # all thresholds location endpoint data
                    equipment_thresholds = json.loads(data['thresholds'])
                except ValueError as e:
                    print('Invalid json: %s' % e)
                    raise
                # getting dictionaries with data of each thresholds parameter
                for threshold in equipment_thresholds:
                    thresholds_params = equipment_thresholds[threshold]
                    thresholds_params_keys = thresholds_params.keys()
                    urgency_keys = list(thresholds_params_keys)
                    for key in urgency_keys:
                        urg_keys.append(key)
                    # make unique urg keys list
                    urg_keys = list(set(urg_keys))
                    urg_keys.sort(reverse=True)
                    return urg_keys

    def _get_thresholds_from_location(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        location_data = self._get_location_data()
        for data in location_data:
            if data['facility_organization'] != 'Ambient Environment':
                equipment_type = data['equipment']
                # json validation
                try:
                    # all thresholds location endpoint data
                    equipment_thresholds = json.loads(data['thresholds'])
                except ValueError as e:
                    print('Invalid json: %s' % e)
                    raise
                if equipment_type != 'B': # and equipment_type != 'PLC':
                    expected_thresholds_keys = list(equipment_thresholds.keys())
                    expected_thresholds_keys.sort()
                    return expected_thresholds_keys

    def _get_bearing_thresholds_from_location(self):
        # GETTING DATA FOR TESTING FROM LOCATION - EXPECTED TESTING DATA AND VALUES
        location_data = self._get_location_data()
        for data in location_data:
            if data['facility_organization'] != 'Ambient Environment':
                equipment_type = data['equipment']
                # json validation
                try:
                    # all thresholds location endpoint data
                    equipment_thresholds = json.loads(data['thresholds'])
                except ValueError as e:
                    print('Invalid json: %s' % e)
                    raise
                if equipment_type == 'B':
                    expected_thresholds_keys_bearing = list(equipment_thresholds.keys())
                    expected_thresholds_keys_bearing.sort()
                    return expected_thresholds_keys_bearing

    def test_get_data(self):
        # list of expected major keys in response
        expected_major_keys = ['critical',
                               'need_attention',
                               'normal']
        expected_major_keys.sort()

        # list of expected keys in sections, if data exists
        expected_sensor_keys = [
            'conveyor',
            'equipment',
            'location',
            'right_menu_label',
            'sensor',
            'standards_score',
            'tag',
            'thresholds',
            'hf_rms_acceleration_x_perc95',
            'hf_rms_acceleration_z_perc95',
            'rms_velocity_x_perc95',
            'rms_velocity_z_mean',
            'temperature_perc95',
            'colors'
        ]
        expected_sensor_keys.sort()

        conveyors = self._get_conveyors_from_location()
        conveyor = random.choice(conveyors)
        self.assertIsNot(conveyor, '', 'Fail. Conveyor should not be empty.')

        urgency_params = self._get_urg_params_from_location()
        expected_thresholds_keys = self._get_thresholds_from_location()
        expected_thresholds_keys_bearing = self._get_bearing_thresholds_from_location()

        # generate url for test conveyor
        url = self._site_url('right-menu', conveyor=conveyor)
        cust_response = self._session.get(url)
        # status code verification for each conveyor
        self.assertEqual(cust_response.status_code, 200, 'Fail! Status code error: ')

        # verify if data exists
        cust_resp = cust_response.json()
        self.assertGreater(len(cust_resp), 0, 'Fail! No data exist in response for the customer.')

        # verify quantity of main sections
        cust_resp = cust_response.json()
        self.assertEqual(len(cust_resp), 3, 'Fail! Wrong number of main sections in response.')

        # type of data in response verification
        self.assertTrue(isinstance(cust_resp, dict), 'Fail! Wrong type of data!')

        # major keys verification
        actual_cust_major_keys = list(cust_resp.keys())
        self.assertEqual(actual_cust_major_keys, expected_major_keys,
                         'Fail! Wrong section names. Names should be {}.'.format(expected_major_keys))

        # keys verification for sensor
        for cust_key in actual_cust_major_keys:
            # we will verify data only if it exists in section: critical, need_attention or normal
            if len(cust_resp[cust_key]) > 0:
                # getting data - list of sensors
                cust_sensors = cust_resp[cust_key]
                for cust_sensor in cust_sensors:
                    # get equipment type of each sensor for further tests
                    cust_equipment_type = cust_sensor['equipment']

                    # verify keys for each sensor
                    cust_keys = cust_sensor.keys()
                    cust_sensor_keys = list(cust_keys)
                    cust_sensor_keys.sort()

                    # verify if keys exist for sensors
                    self.assertGreater(len(cust_sensor_keys), 0,
                                       'Fail! Keys do not exist for sensors.')

                    # verify that keys for sensors are correct
                    self.assertEqual(cust_sensor_keys, expected_sensor_keys,
                                     'Fail! Wrong sensors keys for sensor {}. Keys should be {}.'
                                     .format(cust_sensor['sensor'], expected_sensor_keys))

                    # thresholds type verification for each sensor
                    cust_thresholds_data = cust_sensor['thresholds']
                    self.assertTrue(isinstance(cust_thresholds_data, str),
                                    'Fail! Wrong type of data in thresholds!')

                    # getting thresholds keys - params
                    # json validation
                    try:
                        cust_sensors_thresholds = json.loads(cust_thresholds_data)
                    except ValueError as e:
                        print('Invalid json: %s' % e)
                        raise

                    if cust_equipment_type == 'PLC':
                        # thresholds verification for PLCs - should be empty
                        cust_plc_thresholds = list(cust_sensors_thresholds)
                        self.assertEqual(len(cust_plc_thresholds), 0,
                                         'Fail. {} - Thresholds should be empty for PLCs - {}.'
                                         .format(self._site, cust_sensor['sensor']))
                    else:
                        cust_sensor_thresholds_keys = list(cust_sensors_thresholds.keys())
                        # list of thresholds (parameters) for each sensor
                        cust_sensor_thresholds_keys.sort()

                        # thresholds keys verification by equipment type
                        if cust_equipment_type != 'B':
                            self.assertEqual(cust_sensor_thresholds_keys, expected_thresholds_keys,
                                             'Fail! Wrong thresholds keys or keys do not exist. {}. '
                                             'Sensor {} - Keys should be {}.'
                                             .format(self._site, cust_sensor['sensor'], expected_thresholds_keys))
                        else:
                            self.assertEqual(cust_sensor_thresholds_keys, expected_thresholds_keys_bearing,
                                             'Fail! Wrong thresholds keys or keys do not exist. {}. Sensor {} '
                                             '- Keys should be {}.'
                                             .format(self._site, cust_sensor['sensor'],
                                                     expected_thresholds_keys_bearing))

                            # verify each parameter data from thresholds of each sensor
                            for cust_sensor_threshold in cust_sensors_thresholds:

                                # getting dictionaries with data of each thresholds parameter
                                cust_thresholds_params = cust_sensors_thresholds[cust_sensor_threshold]

                                # verify type of data
                                self.assertEqual(type(cust_thresholds_params), dict,
                                                 'Fail. Wrong data type in sensor thresholds.')

                                # verify that data exists in dictionaries
                                self.assertGreater(len(cust_thresholds_params), 0,
                                                   'Fail. No data in sensor thresholds.')

                                # get urgency keys list for each threshold parameter
                                cust_params_urg_keys = list(cust_thresholds_params.keys())
                                cust_params_urg_keys.sort(reverse=True)

                                # verify urgency keys of thresholds parameters of each sensor are correct
                                self.assertEqual(cust_params_urg_keys, urgency_params,
                                                 'Fail. Urgency keys are wrong for sensor thresholds. '
                                                 'Keys should be {}.'.format(urgency_params))
